extends Area2D

enum TYPE {BRICK, RED, YELLOW, GREEN, BOMB, STEEL}

var shape
var sprite
var type setget set_type, get_type

func _ready():
	shape = $shape
	sprite = $sprite
	randomize_type()

func randomize_type():
	var rand_int = randi() % TYPE.size()
	set_type(rand_int)
	
func set_type(newvalue):
	type = newvalue
	var rect_position = Vector2(0, 24 * newvalue)
	var rect_size = Vector2(16, 24)
	var rect = Rect2(rect_position, rect_size)
	sprite.region_enabled = true
	sprite.region_rect = rect
	
func get_type():
	return type
	
func destroy():
	match type:
		BRICK:
			replace_by(load("res://assets/blocks/block.tscn"))
		RED:
			queue_free()
		YELLOW:
			queue_free()
		GREEN:
			queue_free()
		BOMB:
			pass
		STEEL:
			pass
		
