extends KinematicBody2D

enum STATE {RUNNING, JUMP, FALL, IDLE, BREAKING}

const UP = Vector2(0, -1)

var as
var area
var speed = 128
var jump_speed = 128
var gravity = 5
var velocity = Vector2(0,0)
var state

func _ready():
	as = $as
	area = $area
	STATE = IDLE
	as.connect("animation_finished", self, "animation_finished")
	
func  _physics_process(delta):
	velocity.y += gravity
	
	if Input.is_action_pressed("left"):
		velocity.x = -speed
	elif Input.is_action_pressed("right"):
		velocity.x = speed
	else:
		velocity.x = 0
		
	if is_on_floor():
		if Input.is_action_pressed("jump"):
			velocity.y = -jump_speed
	
	set_animation(velocity)
		
	velocity = move_and_slide(velocity, UP)
	
func set_animation(velocity):
	var left = true if Input.is_action_pressed("left") else false
	var right = true if Input.is_action_pressed("right") else false
	var idle = true if (!right and !left) else false
	var on_air = true if !is_on_floor() else false
	var breaking = true if Input.is_action_pressed("break") or (as.animation == "breaking" and state == BREAKING) else false
	
	if left:
		as.flip_h = false
	elif right:
		as.flip_h = true

	if breaking:
		state = BREAKING	
		as.play("breaking")
		return

	if on_air:
		if velocity.y <= 0:
			state = JUMP
			as.play("jump")
		else:
			state = FALL
			as.play("fall")
		return

	if left or right:
		state = RUNNING
		as.play("run")
	else:
		state = IDLE
		as.play("idle")

func animation_finished():
	if as.animation == "breaking":
		state = null
		break_block()
	
func break_block():
	var block_to_destroy
	var blocks = area.get_overlapping_areas()
	
	if !blocks.empty():
		var min_length = null
		
		for b in blocks:
			var length = (b.position - area.position).length()
			length = abs(length)
			
			if min_length == null or length < min_length:
				min_length = length
				block_to_destroy = b

	if block_to_destroy:
		block_to_destroy.destroy()